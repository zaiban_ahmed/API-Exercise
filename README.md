# Flask Kube api  

## Instructions

Git clone the repository 

```sh
  # Bring up application in docker
  docker-compose up -d
 # Load people from test CSV
  curl localhost:5000/load 
 # List people
  curl localhost:5000/people

  Other options:

  # Adding an item
  curl --header "Content-Type: application/json" --request POST --data @test/data.json localhost:5000/people
  # Modifying the item
  curl --header "Content-Type: application/json" --request PUT --data @test/data2.json localhost:5000/people/1
  # After modifying check person has been added/modified 
  curl localhost:5000/people 
  # Delete entry
  curl --header "Content-Type: application/json" --request DELETE localhost:5000/people/1

  # Load your own custom CSV in the test directory and run:
   curl -F 'file=@test/titanic.csv' localhost:5000/csv

  # If you want to make modification to match any specific requirements to the docker-compose, make your changes and run: 
  docker-compose build && docker-compose push
  
  # To shutdown
  docker-compose down
```

## Instructions for deploying to Minikube using Helm

I've added the helm chart in the helm directory.

```
#Go into the helm directory
cd helm/
#Install the helm chart
helm upgrade --install myrelease .

# This will give you the url+port
minikube service --url apiapp

curl {url}:{port}/load
curl {url}:{port}/people

```
